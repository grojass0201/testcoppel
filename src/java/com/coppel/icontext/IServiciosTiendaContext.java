/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coppel.icontext;

import com.coppel.connections.CoppelConnection;
import java.sql.Connection;

/**
 *
 * @author fcolin
 */
public interface IServiciosTiendaContext {
    public void open() throws Throwable;
    public void close();
    public CoppelConnection getConnection();
    public Connection getConnectionJDBC() throws Throwable;
}
