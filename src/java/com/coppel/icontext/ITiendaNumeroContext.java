/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coppel.icontext;

import com.coppel.connections.CoppelConnection;

/**
 *
 * @author fcolin
 */
public interface ITiendaNumeroContext {
    public void open() throws Throwable;
    public void close()throws Throwable;
    public CoppelConnection getConnection();
}
