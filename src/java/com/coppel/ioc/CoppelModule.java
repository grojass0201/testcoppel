/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coppel.ioc;

import com.coppel.context.TiendaNumeroContext;
import com.coppel.context.ServiciosTiendasContext;
import com.coppel.context.initialized.ExistenciaValesConvencion;
import com.coppel.controllers.ValesConvencionController;
import com.coppel.dao.ValesConvencionDAO;
import com.coppel.icontrolles.IValesConvencionController;
import com.coppel.idao.IValesConvencionDAO;
import com.coppel.imodels.IValesConvencionModel;
import com.coppel.models.ValesConvencionModel;
import com.google.inject.AbstractModule;
import com.coppel.icontext.ITiendaNumeroContext;
import com.coppel.icontext.IServiciosTiendaContext;
import com.coppel.context.initialized.IExistenciaValesConvencion;

/**
 *
 * @author fcolin
 */
public class CoppelModule extends AbstractModule{

    @Override
    protected void configure() {
        bind(IValesConvencionController.class).to(ValesConvencionController.class);
        bind(IValesConvencionModel.class).to(ValesConvencionModel.class);
        bind(IValesConvencionDAO.class).to(ValesConvencionDAO.class);
        bind(ITiendaNumeroContext.class).to(TiendaNumeroContext.class).asEagerSingleton();
        bind(IServiciosTiendaContext.class).to(ServiciosTiendasContext.class).asEagerSingleton();
        bind(IExistenciaValesConvencion.class).to(ExistenciaValesConvencion.class);
        
       // bind(IValesConvencionContext.class).to(ValesConvencionContext.class).asEagerSingleton();
       
    }
    
}
