/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coppel.ioc;
import com.coppel.filters.RequestFilter;
import com.google.inject.Guice;
import com.google.inject.Injector;
/**
 *
 * @author fcolin
 */
public class ServiceLocator {
    private Injector injector;
     public <T> T getInstaceCoppelModule(Class<T> returnType){
        
        if (injector == null){
            try{
                injector = Guice.createInjector(RequestFilter.getCommonDAO(Thread.currentThread().getId()),new CoppelModule());
            }catch(Exception x){
             x.getStackTrace();
            }
        }
        return injector.getInstance(returnType);
    }
}
