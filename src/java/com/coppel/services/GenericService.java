/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coppel.services;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import api.tienda.entidades.respuestas.WebResponse;
import com.coppel.entidades.TdCheques;
import com.coppel.icontrolles.IValesConvencionController;
import com.coppel.ioc.ServiceLocator;
import com.coppel.util.CommonsUtil;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.POST;
/**
 * REST Web Service
 *
 * @author fcolin
 */
@Path("vales")
public class GenericService {

    private final String ejecutable = "api.vales";
    @Context
    private UriInfo context;

    /**
     * Creates a new instance of GenericResource
     */
    public GenericService() {
    }
    
        @GET
	@Path("convencion/existencia")
	@Consumes(value = {MediaType.APPLICATION_FORM_URLENCODED})
	@Produces(MediaType.APPLICATION_JSON)
	public Response consultarExistenciaValesConvencion(@Context HttpServletRequest request,
         @DefaultValue("0") @QueryParam("tienda") int tienda,
         @DefaultValue("0") @QueryParam("caja") int caja,
         @DefaultValue("0") @QueryParam("area") String area,
         @DefaultValue("0") @QueryParam("empleado") int empleado,
         @DefaultValue("0") @QueryParam("beneficiario") int beneficiario) throws Throwable             
	{  
             CommonsUtil.addContextToGet(ejecutable, (short)tienda, (short)caja, area ,empleado, "GET (convencion/existencia)", request.getRemoteAddr());
             ServiceLocator serviceLocator = new ServiceLocator();
             WebResponse wr = new WebResponse();
             try{
                 IValesConvencionController iValesConvencionController = serviceLocator.getInstaceCoppelModule(IValesConvencionController.class);
                 wr.setData(iValesConvencionController.consultarExistenciaValesConvencion((short)tienda ,beneficiario));
                 return Response.ok(wr).build();
             }catch(Exception ex){
                 ex.getStackTrace();
                 return Response.serverError().entity(wr).build();
             }
             
        }
        
        @POST
	@Path("convencion/grabartdcheque")
	@Consumes(value = {MediaType.APPLICATION_FORM_URLENCODED})
	@Produces(MediaType.APPLICATION_JSON)
	public Response listaValesConvencion(@Context HttpServletRequest request,
         @DefaultValue("0") @QueryParam("tienda") int tienda,
         @DefaultValue("0") @QueryParam("caja") int caja,
         @DefaultValue("0") @QueryParam("area") String area,
         @DefaultValue("0") @QueryParam("empleado") int empleado,
         @DefaultValue("0") @QueryParam("beneficiario") int beneficiario,
         @DefaultValue("0") @QueryParam("monto") int monto) throws Exception , Throwable
	{  
            CommonsUtil.addContextToGet(ejecutable, (short)tienda, (short)caja, area ,empleado, "POST (convencion/grabartdcheque)", request.getRemoteAddr());
            ServiceLocator serviceLocator = new ServiceLocator();
            
             WebResponse wr = new WebResponse();
             try{
                 /*TdCheques td = new TdCheques();
                 td.setClavesistema(1);
                 td.setCaja((short)caja);
                 td.setMovimiento((short)13);
                 td.setCliente(beneficiario);
                 td.setMonto(monto);
                 td.setBanco("CONVENCION");
                 td.setEmpleado(empleado);
                 td.setCodautorizacionbanco();
                 td.setFactura();
                 td.setIdu_modalidadtarjeta((short)1);
                 
                 IValesConvencionController iValesConvencionController = serviceLocator.getInstaceCoppelModule(IValesConvencionController.class);
                 wr.setData(data);*/
                 return Response.ok(wr).build();
             }catch(Exception ex){
                 return Response.serverError().entity(wr).build();
             }
        }
        
        @GET
	@Path("convencion/grabarvales")
	@Consumes(value = {MediaType.APPLICATION_FORM_URLENCODED})
	@Produces(MediaType.APPLICATION_JSON)
	public Response grabarValesConvencion(@Context HttpServletRequest request,
         @DefaultValue("0") @QueryParam("tienda") int tienda,
         @DefaultValue("0") @QueryParam("caja") int caja,
         @DefaultValue("0") @QueryParam("area") String area,
         @DefaultValue("0") @QueryParam("empleado") int empleado,
         @DefaultValue("") @QueryParam("data") String data) throws Exception , Throwable
	{  
            CommonsUtil.addContextToGet(ejecutable, (short)tienda, (short)caja, area ,empleado, "GET (convencion/grabarvales)", request.getRemoteAddr());
            ServiceLocator serviceLocator = new ServiceLocator();
          
             WebResponse wr = new WebResponse();
             try{
                 wr.setData(data);
                 return Response.ok(wr).build();
             }catch(Exception ex){
                 return Response.serverError().entity(wr).build();
             }
        }
        
        @GET
	@Path("convencion/imprimirvales")
	@Consumes(value = {MediaType.APPLICATION_FORM_URLENCODED})
	@Produces(MediaType.APPLICATION_JSON)
	public Response imprimirValesConvencion(@Context HttpServletRequest request,
         @DefaultValue("0") @QueryParam("tienda") int tienda,
         @DefaultValue("0") @QueryParam("caja") int caja,
         @DefaultValue("0") @QueryParam("area") String area,
         @DefaultValue("0") @QueryParam("empleado") int empleado,
         @DefaultValue("") @QueryParam("data") String data) throws Exception , Throwable
	{  
            CommonsUtil.addContextToGet(ejecutable, (short)tienda, (short)caja, area ,empleado, "GET (convencion/imprimirvales)", request.getRemoteAddr());
            ServiceLocator serviceLocator = new ServiceLocator();
          
             WebResponse wr = new WebResponse();
             try{
                 wr.setData(data);
                 return Response.ok(wr).build();
             }catch(Exception ex){
                 return Response.serverError().entity(wr).build();
             }
        }
        
        

    /**
     * Retrieves representation of an instance of com.coppel.GenericResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public String getXml() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of GenericResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_XML)
    public void putXml(String content) {
    }
}
