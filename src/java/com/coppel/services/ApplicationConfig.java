/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coppel.services;

import com.coppel.filters.FilterConnectionClear;
import com.coppel.filters.RequestFilter;
import java.util.Set;
import javax.ws.rs.core.Application;
import org.glassfish.jersey.jackson.JacksonFeature;

/**
 *
 * @author fcolin
 */
@javax.ws.rs.ApplicationPath("")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
         resources.add(JacksonFeature.class);
        resources.add(RequestFilter.class);
        resources.add(FilterConnectionClear.class);
        addRestResourceClasses(resources);
        //resources.add(JacksonFeature.class);
        //resources.add(RequestFilter.class);
        //resources.add(com.coppel.filters.ServiceLocatorRemoveFilter.class);
        //resources.add(com.coppel.filters.FilterConnectionClear.class);   
        //resources.add(ResponseFilter.class);// no quitar hasta que no exista el end-point de cliente.
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(com.coppel.services.GenericService.class);
    }
    
}
