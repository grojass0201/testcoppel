/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coppel.models;

import com.coppel.entidades.CCtlFoliosDoctosxCobrar;
import com.coppel.entidades.CTdaFoliosSedesol;
import com.coppel.idao.IValesConvencionDAO;
import javax.inject.Inject;
import com.coppel.imodels.IValesConvencionModel;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fcolin
 */
public class ValesConvencionModel implements IValesConvencionModel{
    @Inject
    IValesConvencionDAO iValesConvencionDAO;

    @Override
    public List<CTdaFoliosSedesol> consultarExistenciaValesConvencion(short sTienda, int iBeneficiario) throws Throwable {
        try {
                     List<CTdaFoliosSedesol> lCtdaFolios = new ArrayList<>();
                     
                     List<CCtlFoliosDoctosxCobrar> ctlFolios= iValesConvencionDAO.ctlFoliosDoctosxCobrar(sTienda, iBeneficiario);
                     if(ctlFolios.size()>0){
                     for(int i = 0; i < ctlFolios.size(); i++ ){
                        CTdaFoliosSedesol ctdaFolios = iValesConvencionDAO.ctdaFoliosSedesol(ctlFolios.get(i).getNum_doctoxcobrar());
                        if(ctdaFolios.getFoliosedesol() == ctlFolios.get(i).getNum_doctoxcobrar()){
                            lCtdaFolios.add(ctdaFolios);
                        }   
                     }
                     }
                     
                    return lCtdaFolios;
	}
	catch(Throwable e) {
            throw e;
	}
    }
}
