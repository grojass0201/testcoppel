/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coppel.dao;

import com.coppel.entidades.CCtlFoliosDoctosxCobrar;
import com.coppel.entidades.CTdaFoliosSedesol;
import com.coppel.exceptions.DefaultDataTienda;
import com.coppel.icontext.IServiciosTiendaContext;
import com.coppel.idao.IValesConvencionDAO;
import javax.inject.Inject;
import com.coppel.icontext.ITiendaNumeroContext;
import java.util.List;

/**
 *
 * @author fcolin
 */
public class ValesConvencionDAO extends BaseDAO implements IValesConvencionDAO{
     @Inject 
     ITiendaNumeroContext iTiendaNumeroContext;
     
     @Inject
     IServiciosTiendaContext iServiciosTiendaContext;

      @Inject private DefaultDataTienda dataTienda;

    @Override
    public List<CCtlFoliosDoctosxCobrar> ctlFoliosDoctosxCobrar(short sTienda, int iBeneficiario) throws Throwable {
        String script = "select * from ctl_foliosdoctosxcobrar where num_beneficiario = 92129978 and num_tipodocumento = 2;";
        List<CCtlFoliosDoctosxCobrar> lVales = getCollection(CCtlFoliosDoctosxCobrar.class, iTiendaNumeroContext.getConnection(), script);
        return lVales;
    }

    @Override
    public CTdaFoliosSedesol ctdaFoliosSedesol(int iFolioSedesol) throws Throwable {
        String script = "select * from tdafoliossedesol where foliosedesol = 92129979 and status = 2;";
        CTdaFoliosSedesol lfolios = getUniqueResult(CTdaFoliosSedesol.class, iServiciosTiendaContext.getConnection(), script);
        return lfolios;
    }
    
    
     
}
