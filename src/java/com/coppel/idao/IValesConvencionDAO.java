/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coppel.idao;

import com.coppel.entidades.CCtlFoliosDoctosxCobrar;
import com.coppel.entidades.CTdaFoliosSedesol;
import java.util.List;

/**
 *
 * @author fcolin
 */
public interface IValesConvencionDAO {
    public List<CCtlFoliosDoctosxCobrar> ctlFoliosDoctosxCobrar(short sTienda,int iBeneficiario) throws Throwable;
    public CTdaFoliosSedesol ctdaFoliosSedesol(int iFolioSedesol) throws Throwable;
}
