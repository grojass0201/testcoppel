/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coppel.controllers;

import com.coppel.icontrolles.IValesConvencionController;
import com.coppel.imodels.IValesConvencionModel;
import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import com.coppel.context.initialized.IExistenciaValesConvencion;
import com.coppel.entidades.CCtlFoliosDoctosxCobrar;
import com.coppel.entidades.CTdaFoliosSedesol;
import java.util.List;

/**
 *
 * @author fcolin
 */
public class ValesConvencionController extends BaseController implements IValesConvencionController{
    @Inject
    IValesConvencionModel iValesConvencionModel;
    
    @Inject
    IExistenciaValesConvencion iConsultarExistenciaValesConvencion;
    
    @Override
    public List<CTdaFoliosSedesol> consultarExistenciaValesConvencion(short sTienda,int iBeneficiario) throws Throwable {
        try {
         
            iConsultarExistenciaValesConvencion.openContext(sTienda);
         
            return iValesConvencionModel.consultarExistenciaValesConvencion(sTienda, iBeneficiario);
        } catch (Throwable ex) {
            procesarExcepcion(ex);
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        } finally{
                iConsultarExistenciaValesConvencion.closeContext();
        }
    }
     
}
