/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coppel.context;

import com.coppel.connections.CoppelConnection;
import com.coppel.connections.CoppelConnectionsFactory;
import com.coppel.connections.CtlBaseDeDatos;
import java.sql.Connection;
import com.coppel.icontext.IServiciosTiendaContext;

/**
 *
 * @author fcolin
 */
public class ServiciosTiendasContext implements IServiciosTiendaContext {

     private CoppelConnection conexionConfig;

    @Override
    public void open() throws Throwable {
    conexionConfig = new CoppelConnectionsFactory().getConnection(new CtlBaseDeDatos().SERVICIOS_TIENDA_VALES);
    //conexionConfig = new CoppelConnectionsFactory().getConnectionConfigurated( new CtlBaseDeDatos().VENTA_ASISTIDA, (short)33);
    
    }

    @Override
    public void close() {
        conexionConfig.close();
    }

    @Override
    public CoppelConnection getConnection() {
     return conexionConfig;
    }

    @Override
    public Connection getConnectionJDBC() throws Throwable {
    return conexionConfig.getConexion(true);
    }
  

}
