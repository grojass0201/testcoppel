/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coppel.context.initialized;

import com.coppel.icontext.IServiciosTiendaContext;
import com.coppel.icontext.ITiendaNumeroContext;
import javax.inject.Inject;

        

/**
 *
 * @author fcolin
 */
public class ExistenciaValesConvencion implements IExistenciaValesConvencion{

    @Inject
    ITiendaNumeroContext iTiendaNumeroContext;
    @Inject
    IServiciosTiendaContext iServiciosTiendaContext;
    
    @Override
    public void openContext(short tienda) throws Throwable {
        iTiendaNumeroContext.open();
        iServiciosTiendaContext.open();
    }

    @Override
    public void closeContext() throws Throwable{
       iTiendaNumeroContext.close();
       iServiciosTiendaContext.close();
    }
    
}
