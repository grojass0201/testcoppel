/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coppel.context;

import com.coppel.connections.CoppelConnection;
import com.coppel.connections.CoppelConnectionsFactory;
import com.coppel.exceptions.DefaultDataTienda;
import com.google.inject.Inject;
import com.coppel.icontext.ITiendaNumeroContext;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author fcolin
 */
public class TiendaNumeroContext implements ITiendaNumeroContext{
  /**
     * CoppelConnection.
     */
    private CoppelConnection conexionLocal;
  
    /**
     * inyectamos la clase DefaultDataTienda.
     */
    @Inject private DefaultDataTienda dataTienda;
    

    @Override
    public void open() throws Throwable {
    conexionLocal = new CoppelConnectionsFactory()
        .getTiendaConnection(Short.parseShort(dataTienda.getTienda()), true);
    }

    @Override
    public void close() throws Throwable {
     if (conexionLocal != null) {
            conexionLocal.close();
        }
    }

    @Override
    public CoppelConnection getConnection() {
    return conexionLocal;
    }
    
}
