/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coppel.entidades;

/**
 *
 * @author fcolin
 */
public class CTdaFoliosSedesol {
    short tienda;
    int	folio;
    int	foliosedesol;
    short status;
    String fechamovto;
    int monto;
    String nombredocumento;

    public short getTienda() {
        return tienda;
    }

    public void setTienda(short tienda) {
        this.tienda = tienda;
    }

    public int getFolio() {
        return folio;
    }

    public void setFolio(int folio) {
        this.folio = folio;
    }

    public int getFoliosedesol() {
        return foliosedesol;
    }

    public void setFoliosedesol(int foliosedesol) {
        this.foliosedesol = foliosedesol;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public String getFechamovto() {
        return fechamovto;
    }

    public void setFechamovto(String fechamovto) {
        this.fechamovto = fechamovto;
    }

    public int getMonto() {
        return monto;
    }

    public void setMonto(int monto) {
        this.monto = monto;
    }

    public String getNombredocumento() {
        return nombredocumento;
    }

    public void setNombredocumento(String nombredocumento) {
        this.nombredocumento = nombredocumento;
    }
    
    
    
}
