/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coppel.entidades;

/**
 *
 * @author fcolin
 */
public class TdCheques {
   short clavesistema;
   short caja;
   short movimiento;
   int cliente;
   int monto;
   String banco;
   int empleado;
   String fecha;
   String codautorizacionbanco;
   int factura;
   short idu_modalidadtarjeta;

    public short getClavesistema() {
        return clavesistema;
    }

    public void setClavesistema(short clavesistema) {
        this.clavesistema = clavesistema;
    }

    public short getCaja() {
        return caja;
    }

    public void setCaja(short caja) {
        this.caja = caja;
    }

    public short getMovimiento() {
        return movimiento;
    }

    public void setMovimiento(short movimiento) {
        this.movimiento = movimiento;
    }

    public int getCliente() {
        return cliente;
    }

    public void setCliente(int cliente) {
        this.cliente = cliente;
    }

    public int getMonto() {
        return monto;
    }

    public void setMonto(int monto) {
        this.monto = monto;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public int getEmpleado() {
        return empleado;
    }

    public void setEmpleado(int empleado) {
        this.empleado = empleado;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getCodautorizacionbanco() {
        return codautorizacionbanco;
    }

    public void setCodautorizacionbanco(String codautorizacionbanco) {
        this.codautorizacionbanco = codautorizacionbanco;
    }

    public int getFactura() {
        return factura;
    }

    public void setFactura(int factura) {
        this.factura = factura;
    }

    public short getIdu_modalidadtarjeta() {
        return idu_modalidadtarjeta;
    }

    public void setIdu_modalidadtarjeta(short idu_modalidadtarjeta) {
        this.idu_modalidadtarjeta = idu_modalidadtarjeta;
    }

    public void setClavesistema(int i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
   
   
}
