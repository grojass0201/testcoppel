/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coppel.entidades;

/**
 *
 * @author fcolin
 */
public class CCtlFoliosDoctosxCobrar {
    Short num_tipodocumento;
    int num_doctoxcobrar;
    String fec_fechamovto;
    int num_beneficiario;

    public Short getNum_tipodocumento() {
        return num_tipodocumento;
    }

    public void setNum_tipodocumento(Short num_tipodocumento) {
        this.num_tipodocumento = num_tipodocumento;
    }

    public int getNum_doctoxcobrar() {
        return num_doctoxcobrar;
    }

    public void setNum_doctoxcobrar(int num_doctoxcobrar) {
        this.num_doctoxcobrar = num_doctoxcobrar;
    }

    public String getFec_fechamovto() {
        return fec_fechamovto;
    }

    public void setFec_fechamovto(String fec_fechamovto) {
        this.fec_fechamovto = fec_fechamovto;
    }

    public int getNum_beneficiario() {
        return num_beneficiario;
    }

    public void setNum_beneficiario(int num_beneficiario) {
        this.num_beneficiario = num_beneficiario;
    }
}
