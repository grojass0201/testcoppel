/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coppel.icontrolles;

import com.coppel.entidades.CTdaFoliosSedesol;
import java.util.List;

/**
 *
 * @author fcolin
 */
public interface IValesConvencionController {
      public List<CTdaFoliosSedesol> consultarExistenciaValesConvencion(short sTienda,int iBeneficiario) throws Throwable;
}
